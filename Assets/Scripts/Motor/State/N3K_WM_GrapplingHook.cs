﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class N3K_WM_GrapplingHook : BaseState
{
    private Vector3 hookPoint;
    private Vector3 hookNormal;
    private float hookSpeed = 30.0f;
    private float hookDeadzone = 0.5f;

    public override void Construct()
    {
        base.Construct();
        immuneTime = 0.5f;

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width >> 1, Screen.height >> 1));
        if (Physics.Raycast(ray, out hit, 100))
        {
            hookPoint = hit.point;
            hookNormal = hit.normal;
            Debug.DrawRay(ray.origin, ray.direction * Vector3.Distance(ray.origin,hit.point), Color.cyan, 5.0f);
        }
        else
        {
            motor.ChangeState("N3KWalking");
        }
    }

    public override Vector3 ProcessMotion(Vector3 input)
    {
        // Get the direction in between you and the hookpoint
        input = (hookPoint - transform.position).normalized;

        // Multiply with the speed
        input *= hookSpeed;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            motor.ChangeState("N3KAir");
            motor.AirInfluence = input;
            motor.VerticalVelocity = input.y;
        }

        return input;
    }

    public override void PlayerTransition()
    {
        base.Transition();

        // Immune check 
        if (Time.time - startTime < immuneTime)
            return;

        if (Vector3.Distance(transform.position, hookPoint) < hookDeadzone)
        {
            motor.hookPoint = hookPoint;
            motor.hookNormal = hookNormal;
            motor.ChangeState("N3K_WM_EndGrapplingHook");
        }

        if(motor.Grounded())
            motor.ChangeState("N3KWalking");
    }
}
